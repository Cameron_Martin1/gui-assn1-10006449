#COMP6001 - GUI Programming
Cameron Martin - 10006449

##Instructional Tips

1. Open your web browser and head to localhost:5000.
2. Using the three RGB sliders, select (or input) the colour of your preference.
3. When you've decided on the colour you want, navigate to the ENTER button to confirm your colour.
4. When your colour has been confirmed the background will change to accompany your choice of colour. 
5. To reset the screen to default, select the reset button below enter and repeat steps 2-4.

##Report

###Colour Scheme
Using a colour scheme that relates to the page functions was extremely important when designing a user interface.
Colour can be manipulated to attract the users attention and allow the user to focus on certain attributes. It's also important to use a good contrast of colours that do not clash or look bad.
https://ux.stackexchange.com/questions/57145/what-are-good-contrast-colors-for-a-gui-application

###Know the User
An importance of developing an interface is knowing who will be using it. For example if it's children using it, it may be a good idea to incorporate a brighter colour scheme with simplistic design. "Start with customers and work backward.” – Jeff Bezos
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals

###Provide Feedback
Although your interface may be easy to navigate, it needs to always let the user what's happening and inform them of their actions. These actions include changes in state and errors or exceptions that occur also visual cues to show whether the users actions have led to their expected result.
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals

###Simplicity
Unless requested by the customer or user, keep an interface simple and easy to navigate. There's nothing more annoying than an interface hard to understand and navigate. A good idea is to ask yourself "does the user really need this?" in the design process.
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals

###One Primary Action Per Screen
Every Screen should support a single action of real value to the person using it. This could also come under the simplicity. Screens supporting one or two actions may confuse the user which can become frustrating.
http://bokardo.com/principles-of-user-interface-design/

###Great Design is Invisible
Great design should go unnoticed by the user. Meaning whilst using the interface they should not encounter any difficulty or errors. The UI needs to be streamlined and do the job that's required.
http://bokardo.com/principles-of-user-interface-design/

###Consistency Matters
Screen designs and components should not appear consistent with eachother unless they behave consistently with each other. In basic, elements that look the same should behave the same.
http://bokardo.com/principles-of-user-interface-design/

###Formatting Content
Ensure the designed layout fits to the screen of the selected device, the user of the interface should be able to see the primary content without having to go out of their way (zooming or scrolling).
https://developer.apple.com/design/tips/

###Resolution
When placing an asset (image or content) into the interface, be sure to check that it is high resolution and is optimized to the selected device to avoid unwanted blur, distortion or low defintion. 
https://developer.apple.com/design/tips/

####References
https://ux.stackexchange.com/questions/57145/what-are-good-contrast-colors-for-a-gui-application
http://blog.teamtreehouse.com/10-user-interface-design-fundamentals
http://bokardo.com/principles-of-user-interface-design/
https://developer.apple.com/design/tips/